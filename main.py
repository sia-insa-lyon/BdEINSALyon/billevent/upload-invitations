import csv
import json

from billevent import BilleventAPI
from local import liste

if __name__ == '__main__':
    liste_from_csv, header = liste('liste.csv')
    print("Données du CSV:")
    print(' | '.join(header))
    for row in liste_from_csv:
        print(' | '.join(row.values()))
    local_emails = [row['email'] for row in liste_from_csv]
    print('--------------------------')
    billevent = BilleventAPI(host="https://api.billetterie.test.bde-insa-lyon.fr/",
                              username=input("Utilisateur : "),
                             #username=b"orgaif",
                              password=input("Mot de passe: ")
                             #password=b"OrgaIF2017"
                             )
    events = billevent.list_events()
    for event in events:
        print(event['id'], event['name'])
    eventID = input("choisissez un id d'évènement : ")
    #eventID=5
    print("vous avez choisi "+str(eventID))
    #for obj in billevent.liste_invitations():
    #    print(obj)
    print("Liste billevent:")
    billevent_emails = billevent.list_mails()
    for obj in billevent_emails:
        print(obj)
    manquants = [x for x in liste_from_csv if x['email'] not in billevent_emails]
    #manquants = [x for x in billevent.list_mails() if x not in liste_from_csv]
    print("-----------------------------")
    print("Liste manquante")
    [print(json.dumps(x)) for x in manquants]
    print("-----------------------------")

    billevent.upload_invitations(manquants, eventID)

    toutes_les_invitations = billevent.liste_invitations()
    for invit in toutes_les_invitations:
        print("({}/{}) {} : https://billetterie.test.bde-insa-lyon.fr/invitation/{}".format(invit["bought_seats"],
                                       invit["seats"],
                                       invit["email"],
                                       invit["token"]))

