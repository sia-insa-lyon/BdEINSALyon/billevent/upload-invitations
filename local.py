import csv


def liste(fichier):
    liste = []
    with open(fichier, 'r') as file:
        reader = csv.DictReader(file, dialect='excel')
        for row in reader:
            liste.append(row)
    file.close()
    return liste, reader.fieldnames