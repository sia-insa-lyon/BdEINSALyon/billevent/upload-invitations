import json
import logging
from base64 import b64encode

from simple_rest_client.exceptions import ClientError

logger = logging.getLogger('billevent')
from requests.auth import HTTPBasicAuth
from simple_rest_client.api import API, Resource
class InvitationRessource(Resource):
    actions = {
        'create': {'method': 'POST', 'url': 'admin/invitations/'},
        'list': {'method': 'GET', 'url': 'admin/invitations/'},
    }
class EventsRessource(Resource):
    actions = {
        'list': {'method': 'GET', 'url': 'admin/events/'}
    }
class BilleventAPI():
    def __init__(self, host, username, password):
        self.host=host
        basic = b64encode(str.encode(username) + b":" + str.encode(password))
        auth = basic.decode("ascii")
        print('Authorization:Basic '+ auth)
        self.api = API(api_root_url=host + 'api/',
                       headers={'Authorization':'Basic '+ auth, 'Content-Type':'application/json'},
                       timeout=60,
                       json_encode_body=True,
                       append_slash=False)
        self.api.add_resource(resource_name='invitations', resource_class=InvitationRessource)
        self.api.add_resource(resource_name='events', resource_class=EventsRessource)

    def liste_invitations(self):
        return self.api.invitations.list().body

    def list_mails(self, liste_invitations=None):
        if liste_invitations is None:
            liste_invitations = self.liste_invitations()
        liste_mails= []
        for obj in liste_invitations:
            liste_mails.append(obj.get('email', None))
        return liste_mails

    def upload_invitations(self, liste, event):
        for invitation in liste:
            try:
                invitation['event_id'] = event
                response = self.api.invitations.create(body=invitation)
                if response.status_code != 201 or response.status_code != 200:
                    logger.warning("Erreur "+response.status+" pour "+json.dumps(invitation))
            except ClientError as e:
                logger.exception(json.dumps(invitation))

    def list_events(self):
        return self.api.events.list().body